<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UController;
use App\Http\Controllers\RedirectController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return view('welcome');
})->name("welcome");


// Route::post('/api/loginUser',[UController::class,'loginUser'])->name('loginUser')->middleware('authLoggedIn');

// Route::get('/api/sigup',[UController::class,'sigup'])->name("sigup");

// Route::get('/api/logoutUser',[UController::class,'logoutUser'])->name("logoutUser");

// Route::get('/api/checkSession',[UController::class,'checkSession'])->name("checkSession");
// Route::get('/api/testEventListener',[UController::class,'testEventListener']);

// Route::post('/api/loginAdmin',[UController::class,'loginAdmin'])->name('loginAdmin')->middleware('authLoggedIn');

// Route::get('/api/logoutAdmin',[UController::class,'logoutAdmin'])->name('logoutAdmin');

//csrf

// Route::get('/csrf-token', function() {
//     return Response::json(['csrf_token' => csrf_token()]);
// });

//Directinal
// Route::get('/checkLogged')->name('checkLogged')->middleware('checkloggedIn');

Route::get('/loginf',[RedirectController::class,'fLoginU'])->name('redirectLoginU')->middleware('authLoggedIn');
Route::get('/login',[RedirectController::class,'fLoginA'])->name('redirectLoginA')->middleware('authLoggedIn');
Route::get('/home',[RedirectController::class,'rHome'])->name('rHome');
Route::get('/admin',[RedirectController::class,'rAdmin'])->name('rAdmin');

