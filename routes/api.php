<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UController;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
Route::post('/loginU',[UController::class,'loginUser'])->middleware('authLoggedIn');
Route::post('/loginA',[UController::class,'loginAdmin'])->middleware('authLoggedIn');
Route::post('/sigup',[UController::class,'sigup']);


Route::get('/logoutA',[UController::class,'logoutAdmin']);
Route::get('/logoutU',[UController::class,'logoutUser']);
Route::get('/authCheck',[UController::class,'authCheck']);
Route::get('/authAdmin',[UController::class,'authAdmin']);
Route::get('/getSession',[UController::class,'getSession']);
