<!-- resources/views/home.blade.php -->

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Trang chủ</title>
</head>
<body>
    <h2>Chào mừng bạn đến với trang chủ</h2>
    <div>
        <h3>Đăng nhập</h3>
        <ul>
        <li><a href="#" onclick="redirectToLogin('{{ route('redirectLoginU') }}', event)">Đăng nhập user</a></li>
        <li><a href="#" onclick="redirectToLogin('{{ route('redirectLoginA') }}', event)">Đăng nhập admin</a></li>
        </ul>
    </div>

<script>
function redirectToLogin(loginRoute, event) {
    event.preventDefault();
    window.location.href = loginRoute;
}
       
</script>
</body>
</html>
