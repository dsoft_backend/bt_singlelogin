<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Home</title>
</head>
<body>
    
    Hello! Welcome here! i'm the admin page...!
    @if(Auth::guard('custom')->check())
        <p>Hello, {{ Auth::guard('custom')->user()->name }}!</p>
    @else
        <p>Hello, guest! Your login session has expired, please log in again</p>
    @endif


    <button onclick="redirectToLogin(event)">Logout</button>

    <script>
   function redirectToLogin(event) {
    event.preventDefault();

    fetch("http://localhost/api/logoutA")
        .then(response => response.json())
        .then(data => {
            if (data.message) {
                window.location.href = "{{ route('welcome') }}";
            }
        })
        .catch(error => {
            console.log(error.message);
        });
}

</script>
</body>
</html>