<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="{{ asset('css/toast.css') }}">
    <link rel="stylesheet" href="{{ asset('images/icons/favicon.ico') }}">
    <link rel="stylesheet" href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/animate/animate.css') }}">
    <script src="https://kit.fontawesome.com/0284d1fcc6.js" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="{{ asset('vendor/css-hamburgers/hamburgers.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/animsition/css/animsition.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/select2/select2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/daterangepicker/daterangepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('css/until.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    <title>Đăng nhập</title>
</head>
<body>
<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form id="login-form" class="login100-form validate-form">
					<span class="login100-form-title p-b-26">
						Welcome
					</span>
					<span class="login100-form-title p-b-48">
						<i class="zmdi zmdi-font"></i>
					</span>

					<div class="wrap-input100 validate-input" data-validate = "Valid email is: a@b.c">
						<input class="input100" type="text" id="email" name="email">
						<span class="focus-input100" data-placeholder="Email"></span>
					</div>

					<div class="wrap-input100 validate-input" data-validate="Enter password">
						<span class="btn-show-pass">
							<i class="zmdi zmdi-eye"></i>
						</span>
						<input class="input100" type="password" id="password" name="password">
						<span class="focus-input100" data-placeholder="Password"></span>
					</div>

					<div class="container-login100-form-btn">
						<div class="wrap-login100-form-btn">
							<div class="login100-form-bgbtn"></div>
							<button class="login100-form-btn" type="submit">
								Login
							</button>
						</div>
					</div>

					<div class="text-center p-t-115">
						<span class="txt1">
							Don’t have an account?
						</span>

						<a class="txt2" href="#">
							Sign Up
						</a>
					</div>
				</form>
			</div>
		</div>
	</div>
    <div id="tagtoast">
    </div>
    <script src="{{ asset('js/toast.js') }}"></script>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var loginForm = document.getElementById('login-form');
            loginForm.addEventListener('submit', function(event) {
                event.preventDefault();

                var formData = new FormData(loginForm);
                
                fetch("http://localhost/api/loginA", {
                    method: "POST",
                    credentials:"include",
                    body: formData
                })
                .then(response => {
                    if(response.status === 419){
                        toast({
                                title: "Request to reload the page!",
                                content: "There are some invalid operations so please reload the page and try again!",
                                typeicon: "error",
                                duration: 8000 
                            })  
                        }
                        return response.json();
                })
                .then(data => {
                    if(data.message){
                        console.log(data.message)
                        if(data.message === 'loginSuccess'){
                            window.location.href = "{{ route('rAdmin') }}";
                        }
                        else if(data.message === 'error_1'){
                            
                            toast({
                                title: "Login failed",
                                content: "The account or password you entered is incorrect",
                                typeicon: "error",
                                duration: 8000 
                            })  
                        }else if(data.message === 'error_2'){

                            toast({
                                title: "Login is not allowed",
                                content: "The account you're logged into is somewhere else!",
                                typeicon: "error",
                                duration: 8000 
                            })  
                        }
                        else if(data.message === 'error_3'){

                            toast({
                                title: "Not have permission to Login!",
                                content: "Your account does not have permission to log in to this site!",
                                typeicon: "error",
                                duration: 8000 
                            })  
                        }
                        

                        const dltoast = document.querySelectorAll('.toast-exit');
                        console.log("button_exit: ",dltoast)
                        dltoast.forEach(element => {
                        element.onclick = function(){
                        console.log("is click exits")
                        element.parentElement.remove('toast')
                  }
                })        
                    }else{
                        console.log("error...")
                    }
                  
                })
                .catch(error => {
                    toast({
                                title: "Request to reload the page!",
                                content: "There are some invalid operations so please reload the page and try again!",
                                typeicon: "error",
                                duration: 8000 
                            })  
                });
            });
        });
    </script>

<script src="{{ asset('vendor/jquery/jquery-3.2.1.min.js') }}"></script>
<script src="{{ asset('vendor/animsition/js/animsition.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/popper.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('vendor/select2/select2.min.js') }}"></script>
<script src="{{ asset('vendor/daterangepicker/moment.min.js') }}"></script>
<script src="{{ asset('vendor/daterangepicker/daterangepicker.js') }}"></script>
<script src="{{ asset('vendor/countdowntime/countdowntime.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
</body>
</html>
