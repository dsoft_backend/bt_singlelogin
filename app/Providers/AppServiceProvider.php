<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Session\CustomDatabaseSessionHandler;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     */
    public function register(): void
    {
        // $this->app->bind('session.handler', CustomDatabaseSessionHandler::class);
    }

    /**
     * Bootstrap any application services.
     */
    public function boot(): void
    {
        //
    }
}
