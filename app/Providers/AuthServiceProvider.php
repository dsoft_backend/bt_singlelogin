<?php

namespace App\Providers;

use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Session\CustomDatabaseSessionHandler;
use App\Guards\CustomGuardAdmin;
use Illuminate\Session\SessionManager;
use Illuminate\Auth\SessionGuard;
// use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The model to policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        //
    ];

    /**
     * Register any authentication / authorization services.
     */
    public function boot(): void
    {


        // Auth::extend('custom', function ($app, $name, array $config) {
        //     $userProvider = new CustomGuardAdmin($app['hash'], $config['provider'],$app['session']);
        //     $sessionManager = $app->make(SessionManager::class);
        //     $session = new SessionGuard('custom', $userProvider, $sessionManager->driver());
        //     return $session;
        // });
        Auth::extend('custom', function ($app, $name, array $config) {
            $userProvider = $app['auth']->createUserProvider($config['provider']);
            $sessionManager = $app->make(SessionManager::class);
            $session = new SessionGuard($name, $userProvider, $sessionManager->driver());
            return $session;
        });

        Session::resolved(function ($session) {
            $session->extend('custom_datass', function ($app) {
                $table = $app['config']['session.table'];
                $lifetime = $app['config']['session.lifetime'];
                $connection = $app['db']->connection($app['config']['session.connection']);
                return new CustomDatabaseSessionHandler($connection, $table, $lifetime, $app);
            });
        });
    }
}
