<?php

namespace App\Guards;

use Illuminate\Auth\SessionGuard;
use Illuminate\Contracts\Auth\UserProvider;

class CustomGuardAdmin extends SessionGuard
{
    public function __construct($name, UserProvider $provider, $session, $request = null)
    {
        parent::__construct($name, $provider, $session, $request);
    }
    public function userId()
    {
        return $this->user() ? $this->user()->getAuthIdentifier() : null;
    }
    public function retrieveByToken($identifier, $token)
    {
        $session = $this->provider->retrieveByToken($identifier, $token);

        if ($session) {
            $session->user_id = $identifier;
        }

        return $session;
    }
}
