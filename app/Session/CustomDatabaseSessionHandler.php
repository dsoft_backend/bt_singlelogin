<?php

namespace App\Session;

use Illuminate\Contracts\Auth\Guard;
use Illuminate\Session\DatabaseSessionHandler;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;
use App\Models\SessionModel;
class CustomDatabaseSessionHandler extends DatabaseSessionHandler
{
    /**
     * Add the user information to the session payload.
     *
     * @param  array  $payload
     * @return $this
     *
     */
    public function write($sessionId, $data): bool
    {   
        $payload = $this->getDefaultPayload($data);

        $adminId = null;
        $this->gc(config('session.lifetime')*60);
        if(Auth::guard('custom')->check()){
            $user_custom = Auth::guard('custom')->user();
            $user_id = $user_custom->id;
            $exitstingSession = SessionModel::where('user_id', $user_id)->where('site','admin')->first();
            if(!$exitstingSession || $user_custom->site == 'user'){
                $adminId = true;
            }
        }
        if (! $this->exists) {
            $this->read($sessionId); 
        }

        if(!$adminId && !$this->userId()){

           
        }else{
        
            if ($this->exists) {
                $this->performUpdate($sessionId, $payload);
            } else {
                $this->performInsert($sessionId, $payload);
            }
    
        }
        return $this->exists = true;
    }


    protected function addUserInformation(&$payload)
    {
        if ($this->container->bound(Guard::class)) {

            if(Auth::check()){
                $payload['user_id'] = $this->userId();
                $payload['site'] = 'user';
            }
            
            if(Auth::guard('custom')->check()){
                $user_custom = Auth::guard('custom')->user();
                if($user_custom->site == 'admin') $payload['user_id'] = $user_custom->id;

                $payload['site'] = 'admin';
            }
        }

        return $this;
    }
}