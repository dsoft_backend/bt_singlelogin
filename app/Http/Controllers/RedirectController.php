<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class RedirectController extends Controller
{
    public function fLoginU(){
        return view('forms/fLoginU');
    }

    public function fLoginA(){
        return view('forms/fLoginA');
    }
    public function rHome(){
        return view('home');
    }
    public function rAdmin(){
        return view('admin');
    }
}
