<?php

/**
 * UController.php
 *
 * This file contains the definition of the UController class,
 * which handles user-related actions in the application.
 *
 * @category    Controllers
 * @package     myAppLoginS
 * @author      Your Name
 * @license     https://opensource.org/licenses/MIT   MIT License
 * @link        http://example.com
 */

namespace App\Http\Controllers;

use App\Enums\StatusEnum;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Cookie;
use Carbon\Carbon;
use App\Events\NewCustomerAdded;
use App\Models\Customer;
use App\Models\User;
use App\Models\SessionModel;
use Exception;

/**
 * UController.php
 *
 * is a file that contains login and logout processing for site admins and users
 */
class UController extends Controller
{

    /**
     * loginUser
     *
     * is the method of handling login for site user
     */
    public function loginUser(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $credentials = [
            'email' => $email,
            'password' => $password,
        ];
        if (Auth::attempt($credentials)) {
            $user_id = Auth::id();
            $exitstingSession = SessionModel::where('user_id', $user_id)->where('site', 'user')->first();

            if ($exitstingSession) {
                $expirationTime = config('session.lifetime') * 60;
                $lastActivity = Carbon::parse($exitstingSession->last_activity);
                $timePass = Carbon::now()->diffInSeconds($lastActivity);
                if ($timePass > $expirationTime) {
                    return response(['message' => 'loginSuccess'])->header('Content-Type', 'application/json');
                } else {
                    Auth::logout();
                    return response(['message' => 'error_2','detail'=>'The account youre logged into is somewhere else'])->header('Content-Type', 'application/json');
                }
            } else {
                return response(['message' => 'loginSuccess'])->header('Content-Type', 'application/json');
            }
        } else {
            return response(['message' => 'error_1','detail'=>'The account or password you entered is incorrect'])->header('Content-Type', 'application/json');
        }
    }
        /**
     * loginAdmin
     *
     * is the method of handling login for site admin
     */
    public function loginAdmin(Request $request)
    {
        $email = $request->input('email');
        $password = $request->input('password');
        $credentials = [
            'email' => $email,
            'password' => $password,
        ];
        if (Auth::guard('custom')->attempt($credentials)) {
            
            $admin = Auth::guard('custom')->user();
            $id = $admin -> id;
            $site = $admin->site;

            if ($site == 'admin') {
                $exitstingSession = SessionModel::where('user_id', $id)->where('site', 'admin')->first();
                if ($exitstingSession) {

                    $expirationTime = config('session.lifetime') * 60;
                    $lastActivity = Carbon::parse($exitstingSession->last_activity);
                    $timePass = Carbon::now()->diffInSeconds($lastActivity);
                    if ($timePass > $expirationTime) {
                        return response(['message' => 'loginSuccess'])->header('Content-Type', 'application/json');
                    } else {
                        Session::flush();
                        Cookie::queue(Cookie::forget('laravel_session'));
                        return response(['message' => 'error_2','detail'=>'The account youre logged into is somewhere else'])->header('Content-Type', 'application/json');
                    }
                } else {
                    return response(['message' => 'loginSuccess'])->header('Content-Type', 'application/json');
                }
            } else {
                Auth::logout();
                Session::flush();
                Cookie::queue(Cookie::forget('laravel_session'));
                return response(['message' => 'error_3','detail' =>'Your account does not have permission to log in to this site!'])->header('Content-Type', 'application/json');
            }
        } else {
            return response(['message' => 'error_1','detail'=>'The account or password you entered is incorrect'])->header('Content-Type', 'application/json');
        }
    }


    /**
     * sigup
     */
    public function sigup(Request $request)
    {
        try{
        $name = $request->input('name');
        $site = $request->input('site');
        $email = $request->input('email');
        $password = $request->input('password');
        User::create(
            [
            'name' =>  $name,
            'email' => $email,
            'site'  => 'admin',
            'password' => Hash::make($password),
            ]
        );
    }catch(Exception $e){
        return response(['exception' => $e])->header('Content-Type', 'application/json');
    }
    }



    /**
     * is the logout handling method for site users
     */
    public function logoutUser()
    {
        if (Auth::check()) {
            $user_id = Auth::id();
            SessionModel::where('user_id', $user_id)->where('site', 'user')->delete();
            Auth::logout();
            return response(['message' => 'logoutSuccess'])->header('Content-Type', 'application/json');
        } else {
            return response(['message' => 'ss_not_auth'])->header('Content-Type', 'application/json');
        }
       
    }


    /**
     * is the logout handling method for site admin
     */
    public function logoutAdmin()
    {
        if (Auth::guard('custom')->check()) {
            $id = Auth::guard('custom')->user()->id;
            SessionModel::where('user_id', $id)->where('site', 'admin')->delete();
            Cookie::queue(Cookie::forget('laravel_session'));
        } else {
            return response(['message' => 'logoutFailed'])->header('Content-Type', 'application/json');
        }
        return response(['message' => 'logoutSuccess'])->header('Content-Type', 'application/json');
    }
    
 /**
     * is the logout handling method for site admin
     */
    public function authCheck(){
        if(Auth::check()){
            return response(['message' => 'oce auth'])->header('Content-Type', 'application/json');
        }else{
            return response(['message' => 'no auth'])->header('Content-Type', 'application/json');
        }
    }

    public function authAdmin(){
        if(Auth::guard('custom')->check()){
            return response(['message' => 'oce auth'])->header('Content-Type', 'application/json');
        }else{
            return response(['message' => 'no auth'])->header('Content-Type', 'application/json');
        }
    }

    public function getSession(){
        $allValues = Session::all();
        return response(['message' => $allValues ,'Lifetime' =>config('session.lifetime')])->header('Content-Type', 'application/json');

    }
    private function createCustomCookie($name, $value, $minutes)
    {
        Cookie::queue($name, $value, $minutes);
    }
}

