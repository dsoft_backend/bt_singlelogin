<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Auth;

class AuthLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::check()){

            // return response(['message' => 'auth is exits'])->header('Content-Type', 'application/json');
            return redirect('/home');
            
        }
        if(Auth::guard('custom')->check()){

            // return response(['message' => 'auth is exits'])->header('Content-Type', 'application/json');
            return redirect('/admin');
            
        }
        return $next($request);
        // return response(['message' => 'auth is not exits'])->header('Content-Type', 'application/json');
    }
}
