<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;

class CheckLoggedIn
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        if(Auth::check()){
            
            return response(['message' => 'error_1'])->header('Content-Type', 'application/json');
            
        }
        if(Auth::guard('custom')->check()){
            
            // return redirect('/admin');
            return response(['message' => 'error_2'])->header('Content-Type', 'application/json');
        }
            
        return response(['message' => 'default'])->header('Content-Type', 'application/json');

        
    }
}
