
# Tuan: BT xây dựng chức năng Single Login .

Yêu Cầu: 
- single Login
- custom auth guard của laravel
- sử dụng git, đúng rule, cài full lint để check PSR
- sử dụng authentication by session base cookie

Mô tả:
- sẽ có hai loại tài khoản , user và admin
- đối với mỗi loại tài khoản thì tại mỗi thời điểm, mỗi tài khoản chỉ đăng nhập trên một thiết bị.
- đối với loại tài khoản user thì chỉ có thể đăng nhập 1 site và tại mỗi thời điểm chỉ đăng nhập 1 site trên một thiết bị.
- đối với tài khoản admin, thì có thể đăng nhập hai site tại một thời điểm, nhưng mỗi site tại một thời điểm cũng chỉ đăng nhập trên một thiết bị.

## file database trong thư mục App\FileDB

## Demo hinh anh

### đăng nhập với loại tài khoản admin vào site user
![icon_logo](https://mcusercontent.com/83e448ffef2b662c110cebf77/images/70e46e7e-254c-c18d-3c73-0ab3c8debe52.png)
![icon_logo](https://mcusercontent.com/83e448ffef2b662c110cebf77/images/d4187658-c18c-03a8-7c62-482fd161b92d.png)

### Tiếp tục đăng nhập tài khoản này và trên cùng site user vào một thiết bị khác
![icon_logo](https://mcusercontent.com/83e448ffef2b662c110cebf77/images/c355bd85-c8d5-6969-149f-ab3cf91a038f.png)
#### sẽ nhận được thông báo đạng tài khoản đã đăng nhập nơi khác và không cho phép đăng nhập

### nhưng cũng tiếp tục đăng nhập tài khoản này với site là admin
![icon_logo](https://mcusercontent.com/83e448ffef2b662c110cebf77/images/be585668-3a15-c748-8f75-529848a7abc0.png)
![icon_logo](https://mcusercontent.com/83e448ffef2b662c110cebf77/images/5281f0d1-d979-77ab-1e5b-17904ccde385.png)
#### thì có thể thực hiện được vì cùng một tài khoản nhưng nó đang đăng nhập trên một site khác





##### ----------------------------------Chúc bạn một ngày tốt lành---------------------------------------
